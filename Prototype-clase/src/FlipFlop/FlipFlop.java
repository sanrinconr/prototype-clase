package FlipFlop;

public class FlipFlop {

	private int dato;
	
	public FlipFlop(int dato) {
		this.dato=dato;
	}
	
	public int getDato() {
		return dato;
	}
	public void setDato(int dato) {
		this.dato=dato;
	}
}
