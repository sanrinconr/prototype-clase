package Usuario;

import FlipFlop.FlipFlop;

public class Usuario {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		FlipFlop dato1 = new FlipFlop(1);
		FlipFlop dato2 = new FlipFlop(2);

		System.out.println("Dato1: " + dato1.getDato());
		System.out.println("Dato2: " + dato2.getDato());
		System.out.println("");
		dato2 = dato1;

		dato2.setDato(5);
		dato1.setDato(1);
		System.out.println("Dato1: " + dato1.getDato());
		System.out.println("Dato2: " + dato2.getDato());

	}

}
